package palit.suchitto.producerconsumer.blockingqueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * PROBLEM STATEMENT: There are two processes # A Producer # And a Consumer.
 * 
 * They share a common buffer with a limited capacity. Producer produces data
 * and stores in the buffer. Consumer consumes data and removes it from the
 * buffer. The two processes run in parallel.
 * 
 * We need to make sure that the, 
 * # producer will not put data in the buffer when the buffer is full, 
 * # and the consumer will not remove data from buffer buffer is empty.
 * 
 * SOLUTION: The producer and the consumer will communicate with each other.
 * 
 * If the buffer is full the producer will wait to be notified. When the
 * consumer removes data from the buffer, the consumer will notify the producer.
 * After notification, the producer will start refilling the buffer again.
 * 
 * If the buffer is empty the consumer will wait to be notified. When the
 * producer adds data in the buffer it will notify the consumer. After
 * notification, the consumer will start to remove data from the buffer again.
 * 
 * 
 * 
 * @author SuchittoPalit
 *
 */

public class ProducerConsumerBlockingQueueExecutorService {

	public static void main(String[] args) throws InterruptedException {
		BlockingQueue<Integer> blockingQueue = new ArrayBlockingQueue<>(2);
		// ExecutorService executorService = Executors.newFixedThreadPool(2);
		// Following gives access to some additional methods
		ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(2);

		Runnable producerTask = () -> {
			// PID is necessary to create a thread dump.
			// Works only with JDK 9+
			System.out.println("PID: " + ProcessHandle.current().pid());
			int value = 0;
			while (value <= 10) {
				try {
					// Only put blocks.
					blockingQueue.put(value);
					System.out.println("Produced: " + value);
					value++;
					Thread.sleep(1000);
				} catch (InterruptedException ie) {
					ie.printStackTrace();
					// Preserve interrupt status. For explanation see
					// "Java Concurrency in Practice" by Brian Goetz et al.
					// Chapter 7.
					Thread.currentThread().interrupt();
				}
			}
		};

		Runnable consumerTask = () -> {
			int value = 0;
			while (value < 10) {
				try {
					// Only take blocks.
					value = blockingQueue.take();
					System.out.println("Consumed: " + value);
					Thread.sleep(1000);
				} catch (InterruptedException ie) {
					ie.printStackTrace();
					// Preserve interrupt status. For explanation see
					// "Java Concurrency in Practice" by Brian Goetz et al.
					// Chapter 7.
					Thread.currentThread().interrupt();
				}
			}
		};
		threadPoolExecutor.submit(producerTask);
		threadPoolExecutor.submit(consumerTask);

		shutdownAndAwaitTermination(threadPoolExecutor);

	}

	private static void shutdownAndAwaitTermination(ExecutorService pool) {
		pool.shutdown(); // Stop new tasks from being submitted
		try {
			// Wait a while for existing tasks to terminate
			if (!pool.awaitTermination(120, TimeUnit.SECONDS)) {
				pool.shutdownNow(); // Cancel currently executing tasks
				// Wait a while for tasks to respond to being cancelled
				if (!pool.awaitTermination(60, TimeUnit.SECONDS))
					System.err.println("Pool did not terminate");
			}
		} catch (InterruptedException ie) {
			// Cancel if current thread also interrupted
			pool.shutdownNow();
			// Preserve interrupt status
			Thread.currentThread().interrupt();
		}
	}
	
}
