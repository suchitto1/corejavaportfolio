package palit.suchitto.producerconsumer.lock;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Buffer {
	private Queue<Integer> list;
	private int limit;
	private final Lock lock = new ReentrantLock();
	private final Condition blockingPool = lock.newCondition();

	public Buffer(int limit) {
		this.limit = limit;
		this.list = new LinkedList<>();
	}

	public void add(int value) throws InterruptedException {
		try {
			while (list.size() >= limit) {
				if (lock.tryLock()) {
					blockingPool.await();
				}
			}
			list.add(value);
			System.out.println(Thread.currentThread().getName() + " Produced : " + value);
			blockingPool.signalAll();

		} finally {
			lock.unlock();
		}
	}

	public int poll() throws InterruptedException {
		try {
			while (list.size() == 0) {
				if (lock.tryLock()) {
					blockingPool.await();
				}
			}
			int value = list.poll();
			System.out.println(Thread.currentThread().getName() + " Consumed: " + value);
			blockingPool.signalAll();
			return value;
		} finally {
			lock.unlock();
		}
	}
}
