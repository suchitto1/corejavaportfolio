This is a very small code repository to showcase my Core java, Datastructures, 
Algorithms and Concurrency codes to Corporate and Freelance employers. Import 
it as a maven project.

I shall add another repo very shortly to showcase my Restful webservices, Spring, 
Spring Boot, Spring WebFlux codes.
		