package palit.suchitto.problem6Java8;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * PROBLEM STATEMENT:
PROBLEM NAME - Search Engine Query
You are given a list of search engine queries.

The queries are in the form of strings and do not contain any whitespace.

When you encounter the string �top <n>�, then output the top n search terms according 
to the popularity. Popularity is measured by the number of times a particular string 
appears in the list before the current occurrence of the word top. Note that those 
searches having the same popularity should be sorted lexicographically.

Input Format
The first line of input consists of an integer t. This is the number of test cases. 
For each test case, the first line of input contains c, where c is the combined number 
of search queries and top strings. Then c lines follow.

Output Format
For each test case, output contains m lines where m is the number of times top appeared 
in the list. Each line of output contains at most n space separated strings ordered by 
popularity and ties broken lexicographically.

Constraints
1 <= < t <= 100 (This is the number of test cases)

1 <= < c <= 10000 (This is the number of search query strings + top strings)

1 <= n <= 1000 (Number appearing next to top)

1 <= s <= 6 (This is the size of search strings)

Query character set - a-z

Note: No case starts with a top and last string in each case will always be top.

Sample Input
2
8
jobs
jobs
videos
cats
videos
cats
videos
top 3
10
videos
songs
songs
top 1
videos
dogs
jobs
movies
dogs
top 7


Sample Output
videos cats jobs
songs
dogs songs videos jobs movies


Environment
Read from STDIN and write to STDOUT.

Remove package declarations and keep the class name as �solution� (lower case)

Instructions
The dashboard provides two modes.
Test runs your code against public/sample test cases.
Submit runs against private/hidden ones.
Only public/sample test cases and their elaborate "test" results are made available. 
A line by line comparison with expected output is shown. There is no score for passing 
the public test cases. It's only for testing and debugging. For the private/hidden test 
cases, the judging system only shows the exit code, passed status, time consumption, 
memory consumption and score. We expect users to take cues from these values. Only 
making a "submit" will yield a score. Total score is a normalized weighted score over 
all test cases.

SAMPLE STDIN 1: See TestCase01.txt
SAMPLE STDOUT 1 : See Output01.txt

 * 
 * 
 * 
 * 
 * @author Suchitto
 *
 */
public class SkillenzaSocieteGeneralProblem2 {
	public static void main(String[] args) throws Exception {

		try (Scanner in = new Scanner(System.in);) {

			int noOfTestCase = Integer.parseInt(in.nextLine());

			Map<String, Integer> testCases = new HashMap<>();
			Integer topNumber;
			String line;
			for (int i = 0; i < noOfTestCase; i++) {
				int noOfQueries = Integer.parseInt(in.nextLine());
				for (int j = 0; j < noOfQueries; j++) {
					line = in.nextLine();

					if (line.contains(" ")) {
						topNumber = Integer.parseInt(line.substring(4));
						printTops(topNumber, testCases);
						continue;
					}

					testCases.compute(line, (k, v) -> (v == null) ? 1 : v + 1);
				}

				testCases.clear();
			}
		}
	}

	private static void printTops(int topNumber, Map<String, Integer> testCases) {

		StringBuilder topSearchString = new StringBuilder();

		List<Map.Entry<String, Integer>> entries = testCases.entrySet().stream().sorted((o1, o2) -> {
			if (o1.getValue().compareTo(o2.getValue()) > 0) {
				// sort sorts in ascending order.
				// We need in descending order.
				return -1;
			} else if (o1.getValue().compareTo(o2.getValue()) < 0) {
				return 1;
			} else {
				// searches having the same popularity should be
				// sorted lexicographically
				return o1.getKey().compareTo(o2.getKey());
			}
		}).collect(Collectors.toList());

		if (topNumber <= entries.size()) {
			for (int i = 0; i < topNumber; i++) {
				topSearchString.append(entries.get(i).getKey().concat(" "));
			}
		} else {
			for (Entry<String, Integer> entry : entries) {
				topSearchString.append(entry.getKey().concat(" "));
			}
		}

		System.out.println(topSearchString.toString());

	}

}
