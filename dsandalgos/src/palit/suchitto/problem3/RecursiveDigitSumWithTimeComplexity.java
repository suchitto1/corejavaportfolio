package palit.suchitto.problem3;

import java.io.IOException;
import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

/**
 * Problem statement is in the pdf file in the same package. Time limit for all
 * testcases is 50 milliseconds. 
 * 
 * 
 * 
 * 
 * @author Suchitto
 *
 */
public class RecursiveDigitSumWithTimeComplexity {

	private static int computeSuperDigit(String n, int k) {

		BigInteger inputDigit = new BigInteger(n);
		BigInteger multiplier = new BigInteger(String.valueOf(k));

		BigInteger initialDigit = inputDigit.multiply(multiplier);

		String initialDigitString = initialDigit.toString();

		int length = initialDigitString.length();

		while (length > 1) {
			initialDigitString = findSumString(initialDigitString, length);
			length = initialDigitString.length();
		}

		return Integer.parseInt(initialDigitString);
	}

	private static String findSumString(String input, int length) {
		long sum = 0;
		for (int i = 0; i < length; i++) {
			if (i == length - 1) {
				sum = sum + Long.parseLong(input.substring(i));
				continue;
			}
			sum = sum + Long.parseLong(input.substring(i, i + 1));
		}

		return String.valueOf(sum);

	}

	public static void main(String[] args) throws IOException {

		try(Scanner scanner = new Scanner(System.in);){

		String[] inputNumberAndTimesToConcatenate = scanner.nextLine().split(" ");

		Instant start = Instant.now();

		String inputNumber = inputNumberAndTimesToConcatenate[0];

		int timesToConcatenate = Integer.parseInt(inputNumberAndTimesToConcatenate[1]);

		int result = computeSuperDigit(inputNumber, timesToConcatenate);

		System.out.println(result);
		Instant finish = Instant.now();
		long timeElapsed = Duration.between(start, finish).toMillis();
		System.out.println("TIME: " + timeElapsed + " MilliSeconds");	
	}
	}
}
