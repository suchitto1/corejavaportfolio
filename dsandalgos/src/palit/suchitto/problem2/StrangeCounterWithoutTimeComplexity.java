package palit.suchitto.problem2;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

/*
 * Problem statement is in the pdf file in the same package.
 * Time limit for all testcases is 50 milliseconds. 
 * 
 * NOTE:This solution is elegant, simple and correct. But it exceeds time constraint 
 * by huge amount for large inputs like TestCase2 and TestCase3.
 * 
 * @author Suchitto Palit
 */

public class StrangeCounterWithoutTimeComplexity {

	private static long count(long t) {
		long result = 1;
		for (long i = 1; i <= t; i++) {
			if (result == 1) {
				result = i + 2;
				continue;
			}
			result--;
		}
		return result;
	}

	public static void main(String[] args) throws IOException {
		try(Scanner scanner = new Scanner(System.in);){
		long t = scanner.nextLong();
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		Instant start = Instant.now();
		long result = count(t);
		System.out.println(String.valueOf(result));
		Instant finish = Instant.now();
		long timeElapsed = Duration.between(start, finish).toMillis();
		System.out.println("TIME: " + timeElapsed + " MilliSeconds");
	}
}

}
